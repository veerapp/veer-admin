define ["cs!veer/js/app/config"], (Config) ->

  model = Backbone.Model.extend(

    idAttribute: "id"

    urlRoot: Config.API_URL + "garages/"

    initialize: (options) ->
      _.bindAll @, "validate"

    validate: ->

  )
  model
