define ["cs!veer/js/app/config"], (Config) ->

  model = Backbone.Model.extend(

    idAttribute: "id"

    urlRoot: Config.API_URL + "rates/"

    initialize: (options) ->
      _.bindAll @, "validate"

    validate: ->

  )
  model
