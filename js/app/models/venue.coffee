define ["cs!veer/js/app/config"], (Config) ->

  model = Backbone.Model.extend(

    idAttribute: "_id"

    urlRoot: Config.API_URL + "venues/"

    initialize: (options) ->
      _.bindAll @, "validate"

    validate: ->

  )
  model
