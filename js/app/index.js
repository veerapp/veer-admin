(function() {
  requirejs.config({
    waitSeconds: 10,
    paths: {
      cs: "../libs/cs",
      'coffee-script': "../libs/coffeescript.min",
      jquery: "../libs/jquery.min",
      text: "../libs/text",
      moment: "../libs/moment.min",
      underscore: "../libs/underscore.min",
      backbone: "../libs/backbone.min",
      bootstrap: "../libs/bootstrap.min",
      timepicker: "../libs/bootstrap-datetimepicker.min",
      numeral: "../libs/numeral.min",
      backgrid: '../libs/backgrid/lib/backgrid',
      paginator: '../libs/backbone.paginator',
      backgridpaginator: '../libs/backgrid.paginator',
      backgridselectall: '../libs/backgrid.selectall',
      nprogress: '../libs/nprogress',
      cookie: '../libs/jquery.cookie',
      numeric: '../libs/jquery.numeric',
      veer: "../.."
    },
    shim: {
      jquery: {
      	exports: "jquery"
      },
      backbone: {
        deps: ["underscore", "jquery"],
        exports: "Backbone"
      },
      bootstrap: {
        deps: ["jquery"],
        exports: "bootstrap"
      },
      underscore: {
        exports: "_"
      },
      moment: {
        exports: "moment"
      },
      numeral: {
        exports: "numeral"
      },
      backgrid: {
        deps: ["jquery", "backbone"],
        exports: "backgrid"
      },
      paginator: {
        deps: ["backgrid"],
        exports: "paginator"
      },
      backgridpaginator: {
        deps: ["paginator"],
        exports: "backgridpaginator"
      },
      backgridselectall: {
        deps: ["paginator"],
        exports: "backgridselectall"
      },
      timepicker: {
        deps: ["backbone", "bootstrap"],
        exports: "timepicker"
      },
      numeric: {
        deps: ["jquery"],
        exports: "numeric"
      }
    }
  });
  require(["cs!veer/js/app/veer"], function(Veer) {
    return Veer.initialize();
  });
}).call(this);
