define [ "cs!./swapping_router", "cs!views/dashboard", "cs!views/users/user_login",
"cs!views/garages/list/garage_index", "cs!views/garages/garage_show",
"cs!views/users/list/user_index", "cs!views/users/user_show",
"cs!views/garages/garage_create", "cs!views/garages/garage_clone",
"cs!views/garages/garage_delete", "cs!models/garage",
"cs!views/venues/list/venue_index", "cs!views/venues/venue_create", "cs!views/venues/venue_delete", "cs!views/venues/venue_show", "cs!models/venue",
"cs!views/operators/list/operator_index", "cs!views/operators/operator_create", "cs!views/operators/operator_show", "cs!models/operator"],
(SwappingRouter, DashboardView, UserLoginView, GarageIndexView, GarageShowView, UserIndexView, UserShowView,
GarageAddView, GarageCloneView, GarageDeleteView, GarageModel, 
VenueIndexView, VenueAddView, VenueDeleteView, VenueShowView, VenueModel, OperatorIndexView, OperatorAddView, OperatorShowView, OperatorModel) ->

  router = SwappingRouter.extend(

    routes:
      "user/login": "login"
      "user/logout": "logout"
      "user/signup": "signup"

      "operators": "operators"
      "operators/add": "add_operator"
      "operators/:id": "show_operator"

      "garages": "garages"
      "garages/add": "add_garage"
      "garages/:id/clone": "clone_garage"
      "garages/:id/delete": "delete_garage"
      "garages/:id": "show_garage"

      "venues": "venues"
      "venues/add": "add_venue"
      "venues/:id/delete": "delete_venue"
      "venues/:id": "show_venue"

      "users": "users"
      "users/:id": "show_user"
      "dashboard": "dashboard"
      "*actions" : "login"

    initialize: (options) ->
      @el = "#page-wrapper"
      _.bindAll @, "dashboard"

    login: ->
      @el = "#page2"
      if _.isUndefined(localStorage["email"]) or _.isUndefined(localStorage["password"])
        $("#temp-container").show()
        $("#main-container").hide()
        view = new UserLoginView()
        @swap view
      else
        Veer.routers.mainRouter.navigate "#dashboard", trigger: true

    logout: ->
      localStorage.clear()
      Veer.routers.mainRouter.navigate "#", trigger: true

    operators: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new OperatorIndexView(
        collections: Veer.collections.operators
      )
      @swap view

    add_operator: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new OperatorAddView(
        model: new OperatorModel
      )
      @swap view

    show_operator: (id) ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new OperatorShowView(
        model: Veer.collections.operators.get(id)
      )
      @swap view

    garages: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new GarageIndexView(
        collections: Veer.collections.garages
      )
      @swap view

    add_garage: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new GarageAddView(
        model: new GarageModel
      )
      @swap view

    clone_garage: (id) ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new GarageCloneView(
        model: Veer.collections.garages.get(id)
      )
      @swap view

    delete_garage: (id) ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new GarageDeleteView(
        model: Veer.collections.garages.get(id)
      )
      @swap view

    show_garage: (id) ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new GarageShowView(
        model: Veer.collections.garages.get(id)
      )
      @swap view

    venues: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new VenueIndexView(
        collections: Veer.collections.venues
      )
      @swap view

    add_venue: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new VenueAddView(
        model: new VenueModel
      )
      @swap view

    delete_venue: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new VenueDeleteView(
        model: Veer.collections.venues.get(id)
      )
      @swap view

    show_venue: (id) ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new VenueShowView(
        model: Veer.collections.venues.get(id)
      )
      @swap view

    users: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new UserIndexView(
        collection: Veer.collections.users
      )
      @swap view

    show_user: (id) ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new UserShowView(
        model: Veer.collections.users.get(id)
      )
      @swap view

    dashboard: ->
      @el = "#page-wrapper"
      @is_authenticated()
      $("#temp-container").hide()
      $("#main-container").show()
      view = new DashboardView()
      @swap view

    is_authenticated: ->
      if _.isUndefined(localStorage["email"]) or _.isUndefined(localStorage["password"])
        Veer.routers.mainRouter.navigate "#user/login", trigger: true


  )
  router