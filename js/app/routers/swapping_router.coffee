define [ "cs!veer/js/app/config", "backbone" ], (Config, Backbone) ->

  SwappingRouter = (options) ->
    Backbone.Router.apply this, [options]

  _.extend SwappingRouter::, Backbone.Router::,

    config: Config

    swap: (newView) ->
      unless _.isUndefined(Backbone)
        if Backbone.currentView and Backbone.currentView.leave
          Backbone.currentView.leave()
      else
        Backbone = new Object()
      Backbone.currentView = newView
      $(@el).html(Backbone.currentView.render().el)

  SwappingRouter.extend = Backbone.Router.extend
  SwappingRouter