define [ "backbone" ], (Backbone) ->

  config =

    API_VERSION: "1"

    TIMESTAMP: moment().utc().format()

    DEBUG: true

    INITIALIZED: false

    API_URL: window.location.protocol + "//" + window.location.host + "/api/1/"

    vent: _.extend({}, Backbone.Events)

    log: log

    PAGE_SIZE: 25

  config
