define [ "cs!veer/js/app/config", "cs!models/garage", "cs!collections/veer" ], (Config, GarageModel, VeerCollection) ->

  collection = VeerCollection.extend(

    model: GarageModel

    idAttribute: "_id"

    url: Config.API_URL + "garages"

    initialize: (options) ->
      VeerCollection::initialize.call @
      _.bindAll @, "executeLongPolling"

    comparator: (item) ->
      item.get "name"

  )
  collection