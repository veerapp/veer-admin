define [ "cs!veer/js/app/config", "cs!models/user", "cs!collections/veer" ], (Config, UserModel, VeerCollection) ->

  collection = VeerCollection.extend(

    model: UserModel

    url: Config.API_URL + "users/"

    initialize: (options) ->
      VeerCollection::initialize.call @
      _.bindAll @, "executeLongPolling"

    comparator: (item) ->
      item.get "name"

  )
  collection