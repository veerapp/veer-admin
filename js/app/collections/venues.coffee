define [ "cs!veer/js/app/config", "cs!models/venue", "cs!collections/veer" ], (Config, VenueModel, VeerCollection) ->

  collection = VeerCollection.extend(

    model: VenueModel

    url: Config.API_URL + "venues/"

    initialize: (options) ->
      VeerCollection::initialize.call @
      _.bindAll @, "executeLongPolling"

      
      

    comparator: (item) ->
      item.get "name"

  )
  collection