define [ "cs!veer/js/app/config", "paginator" ], (Config, paginator) ->

  collection = Backbone.PageableCollection.extend(

    config: Config

    longPolling: false

    intervalSeconds: 60

    mode: "server" # page entirely on the server side

    # Initial pagination states
    state:
      pageSize: Config.PAGE_SIZE
      # sortKey: "updated"
      # order: 1
    
    # You can remap the query parameters from `state` keys from
    # the default to those your server supports
    # queryParams:
    #   totalPages: null
    #   totalRecords: null
    #   sortKey: "sort"
      # q: "state:closed repo:jashkenas/backbone"

    parseState: (resp, queryParams, state, options) ->
      totalRecords: resp.total_count

    parseRecords: (resp, options) ->
      resp.items

    initialize: (options) ->
      @THING = if _.isUndefined(@url) then @localStorage.name else @url

    startLongPolling: (intervalSeconds) ->
      Veer.log.info moment().format() + ": start polling [" + @THING + "]" if Config.DEBUG
      @longPolling = true
      @intervalSeconds = intervalSeconds  if intervalSeconds

      @interval_id = setInterval (=>
        @executeLongPolling()
      ), 1000 * @intervalSeconds

    stopLongPolling: ->
      Veer.log.info moment().format() + ": stop polling [" + @THING + "]" if Config.DEBUG
      @longPolling = false
      clearTimeout(@interval_id)

    executeLongPolling: ->
      Veer.log.info moment().format() + ": fetching [" + @THING + "]" if Config.DEBUG
      @fetch
        remove: true
        success: =>
          if window.location.hash is "#error"
            window.location = "/"
          else
            @onFetch
        error: =>
          Veer.routers.mainRouter.navigate "#error", trigger: true
  )
  collection
