define [ "cs!veer/js/app/config", "cs!models/operating.hour", "cs!collections/veer" ], (Config, OperatingHourModel, VeerCollection) ->

  collection = VeerCollection.extend(

    model: OperatingHourModel

    idAttribute: "_id"

    url: Config.API_URL + "garages"

    initialize: (options) ->
      VeerCollection::initialize.call @
      _.bindAll @, "executeLongPolling"

    comparator: (item) ->
      item.get "name"

  )
  collection