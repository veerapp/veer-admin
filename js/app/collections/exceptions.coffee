define [ "cs!veer/js/app/config", "cs!models/exception", "cs!collections/veer" ], (Config, GarageModel, VeerCollection) ->

  collection = VeerCollection.extend(

    model: GarageModel

    idAttribute: "_id"

    url: Config.API_URL + "exceptions"

    initialize: (options) ->
      VeerCollection::initialize.call @
      _.bindAll @, "executeLongPolling"

    comparator: (item) ->
      item.get "name"

  )
  collection