define [ "cs!veer/js/app/config", "cs!models/operator", "cs!collections/veer" ], (Config, OperatorModel, VeerCollection) ->

  collection = VeerCollection.extend(

    model: OperatorModel

    idAttribute: "_id"

    url: Config.API_URL + "operators"

    initialize: (options) ->
      VeerCollection::initialize.call @
      _.bindAll @, "executeLongPolling"

    comparator: (item) ->
      item.get "name"

  )
  collection