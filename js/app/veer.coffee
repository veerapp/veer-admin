define [ "jquery", "underscore", "cookie", "backbone", "backgrid", "bootstrap", "moment", "timepicker", "numeral", "cs!routers/main_router", "cs!collections/garages", "cs!collections/users", "cs!collections/operators", "cs!collections/venues", "cs!veer/js/app/config" ],
($, _, cookie, Backbone, backgrid, bootstrap, moment, timepicker, numeral, MainRouter, GarageCollection, UserCollection, OperatorCollection, VenueCollection, Config) ->

  class VeerApp

    constructor: ->
      window.Veer = {}
      
      Veer.vent = Config.vent

      Veer.API_VERSION = Config.API_VERSION

      Veer.DEBUG = Config.DEBUG
      Veer.API_URL = Config.API_URL

      Veer.log = log
      Veer.log.enableAll()

      Veer.collections = {}
      Veer.collections.venues = new VenueCollection()
      Veer.collections.garages = new GarageCollection()
      Veer.collections.users = new UserCollection()
      Veer.collections.operators = new OperatorCollection()
      
      Veer.routers = {}
      Veer.routers.mainRouter = new MainRouter()

      Veer.log.info moment().format() + ": Welcome to Veer"  if Veer.DEBUG

      unless _.isUndefined(localStorage["email"]) or _.isUndefined(localStorage["password"])
        data =
          email: localStorage["email"]
          password: localStorage["password"]
        Veer.user = data
        $.ajax(
          type: "POST"
          url: Config.API_URL + "users/auth"
          contentType: "application/json"
          data: JSON.stringify(data)
          success: (user) =>
            Veer.user = user

            $.ajaxSetup headers:
              Authorization: "Basic " + btoa(localStorage["email"] + ":" + localStorage["password"])
            
            Veer.log.info moment().format() + ": loading operators ..."  if Veer.DEBUG
            Veer.collections.operators.fetch(
              wait: true
              success: =>
                Veer.log.info moment().format() + ": loading garages ..."  if Veer.DEBUG
                Veer.collections.garages.fetch(
                  wait: true
                  success: =>
                    Veer.log.info moment().format() + ": loading users ..."  if Veer.DEBUG
                    Veer.collections.users.fetch(
                      wait: true
                      success: =>
                        Veer.log.info moment().format() + ": loading venues ..."  if Veer.DEBUG
                        Veer.collections.venues.fetch(
                          wait: true
                          success: =>
                            Veer.routers.mainRouter.navigate "#dashboard", trigger: true
                        )
                    )        
                )
            )

          error: (err) =>
            localStorage.clear()
            Veer.routers.mainRouter.navigate "#user/login", trigger: true
        )
        false
      else
        Veer.routers.mainRouter.navigate "#user/login", trigger: true

      Backbone.history.start()

  initialize = ->
    new VeerApp()

  initialize: initialize