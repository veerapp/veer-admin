define [ "cs!views/veer_view", "text!veer/templates/app/dashboard.html", "nprogress" ], (VeerView, DashboardTemplate, NProgress) ->

  view = VeerView.extend(

    template: _.template DashboardTemplate

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render"

    renderLayout: ->
      $(@el).html @template()
      $("html, body").animate({ scrollTop: 0 }, "fast")

    render: ->
      NProgress.start()
      @renderLayout()
      @renderNavigation()
      NProgress.done()
      @
  )
  view

