define [ "cs!views/veer_view", "text!veer/templates/app/venues/venue_create.html", "cs!views/message_view", "numeric" ],
(VeerView, Template, MessageView, numeric) ->

  view = VeerView.extend(

    template: _.template Template

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render", "successGeo", "errorGeo"
      @views = []

    events:
      "click #venue-submit": "submit"
      "click #cancel": "cancel"

    enable_all: ->
      @venue_name.prop("disabled", false)
      @venue_street.prop("disabled", false)
      @venue_city.prop("disabled", false)
      @venue_state.prop("disabled", false)
      @venue_zip.prop("disabled", false)
      @venue_phone.prop("disabled", false)
      @venue_web.prop("disabled", false)
      @venue_longitude.prop("disabled", false)
      @venue_latitude.prop("disabled", false)
      @venue_active.prop("disabled", false)
      @venue_submit.button("reset")

    disable_all: ->
      @venue_name.prop("disabled", true)
      @venue_street.prop("disabled", true)
      @venue_city.prop("disabled", true)
      @venue_state.prop("disabled", true)
      @venue_zip.prop("disabled", true)
      @venue_phone.prop("disabled", true)
      @venue_web.prop("disabled", true)
      @venue_longitude.prop("disabled", true)
      @venue_latitude.prop("disabled", true)
      @venue_active.prop("disabled", true)
      @venue_submit.button("loading")

    submit: ->
      ADDRESS = @venue_street.val() + " " + @venue_city.val() + ", " + @venue_state.val() + " " + @venue_zip.val()
      $.ajax(
        type: "GET"
        dataType: "jsonp"
        cache: true
        url: "https://www.mapquestapi.com/geocoding/v1/address?key=Fmjtd%7Cluub2q0bll%2C85%3Do5-9u7590&location=" + ADDRESS + "&addressdetails=1"
        success: @successGeo
        error: @errorGeo
      )
      false

    errorGeo: ->
      @enable_all()
      view = new MessageView(
        css: "alert-error"
        message: "Error geocoding address. Please check address info"
        id: "#venue-message"
        stick: true
      )
      view.render()

    successGeo: (geoData) ->
      @disable_all()
      @model.idAttribute = null

      data = {}
      data["name"] = @venue_name.val()
      data["active"] = @venue_active.prop("checked")
      data["geo"] = [ parseFloat(@venue_longitude.val()), parseFloat(@venue_latitude.val()) ]

      data["address"] = [
        street: @venue_street.val()
        city: @venue_city.val()
        state: @venue_state.val()
        zip: @venue_zip.val()
        countryCode: geoData.results[0].locations[0].adminArea1
        geo: [ geoData.results[0].locations[0].latLng.lng, geoData.results[0].locations[0].latLng.lat ]
      ]

      Veer.log.info moment().format() + ": creating venue: " + JSON.stringify(data, null, 2) if Veer.DEBUG
      Veer.collections.venues.create(data,
        wait: true
        success: (data) =>
          Veer.collections.venues.fetch(
            reset: true
            success: =>
              @navigateToVenues()         
          )
        error: (model, err) =>
          @scrollToTop()
          view = new MessageView(
            id: "#venue-message"
            css: "alert-danger"
            message: err.responseText
            stick: true
          )
          view.render()
          @enable_all()
      )

    cancel: ->
      @navigateToVenues()

    renderLayout: ->
      $(@el).html @template()

    render: ->
      VeerView::render.call @

      @venue_name = $(@el).find("#venue-name")
      @venue_active = $(@el).find("#venue-active")
      @venue_web = $(@el).find("#venue-web")
      @venue_street = $(@el).find("#venue-street")
      @venue_city = $(@el).find("#venue-city")
      @venue_state = $(@el).find("#venue-state")
      @venue_zip = $(@el).find("#venue-zip")
      @venue_phone = $(@el).find("#venue-phone")
      @venue_longitude = $(@el).find("#venue-longitude")
      @venue_latitude = $(@el).find("#venue-latitude")
      @venue_submit = $(@el).find("#venue-submit")

      @venue_zip.numeric()
      @venue_longitude.numeric()
      @venue_latitude.numeric()
      
      @
  )
  view

