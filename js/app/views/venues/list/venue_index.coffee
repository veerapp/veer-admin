define [ "cs!views/veer_view", "text!veer/templates/app/venues/list/venue_index.html", "backgridpaginator", "backgridselectall", "nprogress" ], (VeerView, GarageIndexTemplate, backgridpaginator, backgridselectall, NProgress) ->

  view = VeerView.extend(

    template: _.template GarageIndexTemplate

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render"

    events:
      "click #venue-delete": "remove_operator"

    remove_operator: ->
      $(".btn-group").removeClass("open")
      _.each @grid.getSelectedModels(), (model) ->
        model.destroy()
      false

    renderLayout: ->
      $(@el).html @template()

    renderGrid: ->
      columns = [
        {
          name: ""
          cell: "select-row"
          headerCell: "select-all"
        }
        # {
        #   name: "_id" # The key of the model attribute
        #   label: "ID" # The name to display in the header
        #   editable: false # By default every cell in a column is editable, but *ID* shouldn't be
        #   cell: "string"
        # }
        {
          name: "name"
          label: "Name"
          editable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.append $("<a href='#venues/" + encodeURIComponent(@model.get("_id")) + "'>",
                href: @column.get("label")
                class: "uri-cell sortable renderable"
                title: @column.get("label")
                target: ""
              ).text(@model.get("name"))
              @
          )
        }
        {
          name: "address"
          label: "Address"
          editable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.html @model.get("address")[0].street + " " + @model.get("address")[0].city + ", " + @model.get("address")[0].state + " " + @model.get("address")[0].zip
              @
          )
        }
        {
          name: "active"
          label: "Active"
          editable: false
          cell: "boolean"
        }
      ]

      # Initialize a new Grid instance
      @grid = new Backgrid.Grid(
        columns: columns
        collection: Veer.collections.venues
      )

      # Render the grid and attach the root to your HTML document
      $(@el).find(".table-responsive").append @grid.render().el

      # Initialize the paginator
      paginator = new Backgrid.Extension.Paginator(
        
        # If you anticipate a large number of pages, you can adjust
        # the number of page handles to show. The sliding window
        # will automatically show the next set of page handles when
        # you click next at the end of a window.
        windowSize: 20 # Default is 10
        
        # Used to multiple windowSize to yield a number of pages to slide,
        # in the case the number is 5
        slideScale: 0.25 # Default is 0.5
        
        # Whether sorting should go back to the first page
        goBackFirstOnSort: false # Default is true
        collection: Veer.collections.venues
      )

      # Render the paginator
      $(@el).find(".table-responsive").after(paginator.render().el);

    render: ->
      VeerView::render.call @
      NProgress.start()
      @renderGrid()
      NProgress.done()
      @
  )
  view

