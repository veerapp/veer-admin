define [ "cs!views/composite_view", "text!veer/templates/app/message_view.html" ], (CompositeView, MessageTemplate) ->

  view = CompositeView.extend(

    template: _.template MessageTemplate

    initialize: (options) ->
      CompositeView::initialize.call @
      _.bindAll @, "render"
      @message = options.message
      @css = options.css
      @stick = false || options.stick
      @id = if options.id then options.id else "#message"

    render: ->
      $(@id).empty().show()
      $(@id).append(@template(
        message: @message
        css: @css
      ))
      $(@id).delay(3000).fadeOut() unless @stick
      @
  )
  view

