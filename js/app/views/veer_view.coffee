define [ "cs!veer/js/app/config", "cs!views/composite_view", "cs!views/layout/navigation" ], (Config, CompositeView, NavigationView) ->

  view = CompositeView.extend(

    config: Config

    initialize: ->
      CompositeView::initialize.call @
      _.bindAll @, "renderNavigation"

    setTitle: (title) ->
      window.document.title = title

    renderNavigation: ->
      if _.isEqual($(".navbar-header").length, 0)
        view = new NavigationView()
        $("#navigation").html view.render().el

    navigateToVenues: ->
      Veer.routers.mainRouter.navigate "#venues", trigger: true

    navigateToGarages: ->
      Veer.routers.mainRouter.navigate "#garages", trigger: true

    navigateToOperators: ->
      Veer.routers.mainRouter.navigate "#operators", trigger: true

    scrollToTop: ->
      $(window).scrollTop(0)

    newFile: (data, cb) ->
      $.ajax(
        type: "POST"
        dataType: "json"
        data: data
        cache: false
        contentType: false
        processData: false
        headers:
          "Authorization": "Basic " + btoa(Veer.user.email + ":" + Veer.user.password)
        url: "//" + window.location.host + "/api/1/files"
        success: (file) =>
          cb null, file
      )

    updateFile: (id, data, cb) ->
      $.ajax(
        type: "PUT"
        dataType: "json"
        data: data
        cache: false
        contentType: false
        processData: false
        headers:
          "Authorization": "Basic " + btoa(Veer.user.email + ":" + Veer.user.password)
        url: "//" + window.location.host + "/api/1/files/" + id
        success: (file) =>
          cb null, file
      )

    render: ->
      @renderLayout()
      @renderNavigation()
      @scrollToTop()

  )
  view
