define [ "cs!views/composite_view", "text!veer/templates/app/users/list/user_show.html" ], (CompositeView, UserShowTemplate) ->

  view = CompositeView.extend(

    template: _.template UserShowTemplate

    initialize: (options) ->
      CompositeView::initialize.call @
      _.bindAll @, "render"

    renderLayout: ->
      $(@el).html @template()

    render: ->
      @renderLayout()
      @
  )
  view

