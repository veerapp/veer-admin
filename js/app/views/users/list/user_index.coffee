define [ "cs!views/veer_view", "text!veer/templates/app/users/list/user_index.html", "backgridpaginator", "backgridselectall", "nprogress" ], (VeerView, UserIndexTemplate, backgridpaginator, backgridselectall, NProgress) ->

  view = VeerView.extend(

    template: _.template UserIndexTemplate

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render"

    events:
      "click #user-delete": "remove_user"

    remove_user: ->
      $(".btn-group").removeClass("open")
      _.each @grid.getSelectedModels(), (model) ->
        model.destroy()
      false

    renderLayout: ->
      $(@el).html @template()

    renderGrid: ->
      columns = [
        {
          name: ""
          cell: "select-row"
          headerCell: "select-all"
        }
        # {
        #   name: "_id" # The key of the model attribute
        #   label: "ID" # The name to display in the header
        #   editable: false # By default every cell in a column is editable, but *ID* shouldn't be
        #   cell: "string"
        # }
        {
          name: "firstname"
          label: "First Name"
          editable: false
          cell: "string"
        }
        {
          name: "lastname"
          label: "Last Name"
          editable: false
          cell: "string"
        }
        {
          name: "email"
          label: "Email"
          editable: false
          cell: "string"
        }
        {
          name: "phone"
          label: "Phone"
          editable: false
          cell: "string"
        }
      ]

      # Initialize a new Grid instance
      @grid = new Backgrid.Grid(
        columns: columns
        collection: Veer.collections.users
      )

      # Render the grid and attach the root to your HTML document
      $(@el).find(".table-responsive").append @grid.render().el

      # Initialize the paginator
      paginator = new Backgrid.Extension.Paginator(
        
        # If you anticipate a large number of pages, you can adjust
        # the number of page handles to show. The sliding window
        # will automatically show the next set of page handles when
        # you click next at the end of a window.
        windowSize: 20 # Default is 10
        
        # Used to multiple windowSize to yield a number of pages to slide,
        # in the case the number is 5
        slideScale: 0.25 # Default is 0.5
        
        # Whether sorting should go back to the first page
        goBackFirstOnSort: false # Default is true
        collection: Veer.collections.users
      )

      # Render the paginator
      $(@el).find(".table-responsive").after(paginator.render().el);

    render: ->
      NProgress.start()
      @renderLayout()
      @renderNavigation()
      @renderGrid()
      NProgress.done()
      @
  )
  view

