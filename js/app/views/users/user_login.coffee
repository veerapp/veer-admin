define [ "cs!veer/js/app/config", "cs!views/composite_view", "text!veer/templates/app/users/user_login.html", "cs!views/message_view" ], (Config, CompositeView, LoginTemplate, MessageView) ->

  view = CompositeView.extend(

    template: _.template LoginTemplate

    initialize: ->
      _.bindAll @, "render", "submit"

    events:
      "keyup #user-login-email": "email"
      "keyup #user-login-password": "password"
      "click #user-login-submit": "submit"

    enable_all: ->
      @email.prop("disabled", true)
      @password.prop("disabled", true)

    disable_all: ->
      @email.prop("disabled", false)
      @password.prop("disabled", false)


    email: ->
      @validate()

    password: ->
      @validate()

    validate: ->
      if _.isEmpty(@email.val()) or _.isEmpty(@password.val())
        @submit.prop("disabled", true)
      else
        @submit.prop("disabled", false)

    submit: ->
      @email.prop("disabled", true)
      @password.prop("disabled", true)
      @submit.button("loading")

      data =
        email: @email.val()
        password: @password.val()

      $.ajax(
        type: "POST"
        url: Config.API_URL + "users/auth"
        contentType: "application/json"
        data: JSON.stringify(data)
        success: (user) =>
          Veer.user = user
          $.ajaxSetup headers:
            Authorization: "Basic " + btoa(user.email + ":" + user.password)
          localStorage["email"] = user.email
          localStorage["password"] = user.password

          Veer.log.info moment().format() + ": loading operators ..."  if Veer.DEBUG
          Veer.collections.operators.fetch(
            wait: true
            success: =>
              Veer.log.info moment().format() + ": loading garages ..."  if Veer.DEBUG
              Veer.collections.garages.fetch(
                wait: true
                success: =>
                  Veer.log.info moment().format() + ": loading users ..."  if Veer.DEBUG
                  Veer.collections.users.fetch(
                    wait: true
                    success: =>
                      Veer.log.info moment().format() + ": loading venues ..."  if Veer.DEBUG
                      Veer.collections.venues.fetch(
                        wait: true
                        success: =>
                          Veer.routers.mainRouter.navigate "#dashboard", trigger: true
                      )
                  )        
              )
          )

        error: (err) =>
          Veer.log.warn moment().format() + ": invalid credentials w/ username [" + data.email + "] & password [" + data.password + "]" if Veer.DEBUG
          view = new MessageView(
            id: "#user-login-message"
            css: "alert-danger"
            message: "Invalid Credentials"
            stick: true
          )#.css("width", "60px")
          @submit.button("reset")
          @email.prop("disabled", false)
          @password.prop("disabled", false)
          # $(@el).find("#user-login-message").html(view.render().el)
          view.render().el
      )
      false

    renderLayout: ->
      $(@el).html @template(
        data:
          version: Config.VERSION
          debug: Config.DEBUG
      )

    render: ->
      @renderLayout()
      @email = $(@el).find("#user-login-email")
      @password = $(@el).find("#user-login-password")
      @submit = $(@el).find("#user-login-submit")
      @

  )
  view
