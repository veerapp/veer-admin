define ["backbone"], (Backbone) ->

  CompositeView = (options) ->
    @children = _([])
    Backbone.View.apply this, [options]

  _.extend CompositeView::, Backbone.View::,
    leave: ->
      @unbind()
      @remove()
      @_leaveChildren()
      @_removeFromParent()

    renderChild: (view) ->
      view.render()
      @children.push view
      view.parent = this

    renderChildInto: (view, container) ->
      @renderChild view
      $(container).empty().append view.el

    appendChild: (view) ->
      @renderChild view
      $(@el).append view.el

    appendChildTo: (view, container) ->
      @renderChild view
      $(container).append view.el

    prependChild: (view) ->
      @renderChild view
      $(@el).prepend view.el

    prependChildTo: (view, container) ->
      @renderChild view
      $(container).prepend view.el

    _leaveChildren: ->
      @children.chain().clone().each (view) ->
        view.leave()  if view.leave

    _removeFromParent: ->
      @parent._removeChild this  if @parent

    _removeChild: (view) ->
      index = @children.indexOf(view)
      @children.splice index, 1

  CompositeView.extend = Backbone.View.extend
  CompositeView

