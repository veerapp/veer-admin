define [ "cs!views/veer_view", "cs!views/message_view", "text!veer/templates/app/garages/hours/hour_create.html", "numeric" ], (VeerView, MessageView, Template, numeric) ->
  
  view = VeerView.extend(
    
    template: _.template Template
    
    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render", "renderLayout", "cancel"
      @grid = options.grid
      @el = options.el
      @errors = []
      @model = new Backbone.Model()
      
    events:
      "click #hour-weekday": "weekday"
      "click #hour-weekend": "weekend"
      "click #hour-every-day": "every_day"
      "click #hour-monday": "monday"
      "click #hour-tuesday": "tuesday"
      "click #hour-wednesday": "wednesday"
      "click #hour-thursday": "thursday"
      "click #hour-friday": "friday"
      "click #hour-saturday": "saturday"
      "click #hour-sunday": "sunday"
      "change #operating-hour-start-hour": "start_hour"
      "change #operating-hour-stop-hour": "stop_hour"
      "click #garage-create-hour-submit": "submit"
      "click #garage-create-hour-cancel": "cancel"
      
    enable_all: ->
      $(@el).find("#garage-create-hour-cancel").prop("disabled", false)
      $(@el).find("#garage-create-hour-submit").button('reset')
      
    disable_all: ->
      $(@el).find("#garage-create-hour-cancel").prop("disabled", true)
      $(@el).find("#garage-create-hour-submit").button('loading')

    validate: ->

    weekday: ->
      $(@el).find(".weekday").show()
      $(@el).find(".weekend").hide()

      @model.set "hour-monday": $(@el).find("#hour-monday").prop("checked", true).prop("checked")
      @model.set "hour-tuesday": $(@el).find("#hour-tuesday").prop("checked", true).prop("checked")
      @model.set "hour-wednesday": $(@el).find("#hour-wednesday").prop("checked", true).prop("checked")
      @model.set "hour-thursday": $(@el).find("#hour-thursday").prop("checked", true).prop("checked")
      @model.set "hour-friday": $(@el).find("#hour-friday").prop("checked", true).prop("checked")
      @model.set "hour-saturday": $(@el).find("#hour-saturday").prop("checked", false).prop("checked")
      @model.set "hour-sunday": $(@el).find("#hour-sunday").prop("checked", false).prop("checked")

    weekend: ->
      $(@el).find(".weekday").hide()
      $(@el).find(".weekend").show()

      @model.set "hour-monday": $(@el).find("#hour-monday").prop("checked", false).prop("checked")
      @model.set "hour-tuesday": $(@el).find("#hour-tuesday").prop("checked", false).prop("checked")
      @model.set "hour-wednesday": $(@el).find("#hour-wednesday").prop("checked", false).prop("checked")
      @model.set "hour-thursday": $(@el).find("#hour-thursday").prop("checked", false).prop("checked")
      @model.set "hour-friday": $(@el).find("#hour-friday").prop("checked", false).prop("checked")
      @model.set "hour-saturday": $(@el).find("#hour-saturday").prop("checked", true).prop("checked")
      @model.set "hour-sunday": $(@el).find("#hour-sunday").prop("checked", true).prop("checked")

    every_day: ->
      $(@el).find(".weekday").show()
      $(@el).find(".weekend").show()

      @model.set "hour-monday": $(@el).find("#hour-monday").prop("checked", true).prop("checked")
      @model.set "hour-tuesday": $(@el).find("#hour-tuesday").prop("checked", true).prop("checked")
      @model.set "hour-wednesday": $(@el).find("#hour-wednesday").prop("checked", true).prop("checked")
      @model.set "hour-thursday": $(@el).find("#hour-thursday").prop("checked", true).prop("checked")
      @model.set "hour-friday": $(@el).find("#hour-friday").prop("checked", true).prop("checked")
      @model.set "hour-saturday": $(@el).find("#hour-saturday").prop("checked", true).prop("checked")
      @model.set "hour-sunday": $(@el).find("#hour-sunday").prop("checked", true).prop("checked")

    monday: ->
      @model.set "hour-monday": $(@el).find("#hour-monday").prop("checked")
      @validate()

    tuesday: ->
      @model.set "hour-tuesday": $(@el).find("#hour-tuesday").prop("checked")
      @validate()

    wednesday: ->
      @model.set "hour-wednesday": $(@el).find("#hour-wednesday").prop("checked")
      @validate()

    thursday: ->
      @model.set "hour-thursday": $(@el).find("#hour-thursday").prop("checked")
      @validate()

    friday: ->
      @model.set "hour-friday": $(@el).find("#hour-friday").prop("checked")
      @validate()

    saturday: ->
      @model.set "hour-saturday": $(@el).find("#hour-saturday").prop("checked")
      @validate()

    sunday: ->
      @model.set "hour-sunday": $(@el).find("#hour-sunday").prop("checked")
      @validate()

    start_hour: ->
      @model.set "hour-start-hour": $(@el).find("#operating-hour-start-hour").data("DateTimePicker").getDate().format("HH:mm")
      @validate()

    stop_hour: ->
      @model.set "hour-stop-hour": $(@el).find("#operating-hour-stop-hour").data("DateTimePicker").getDate().format("HH:mm")
      @validate()

    submit: ->
      data =
        id: _.uniqueId()
        active: $(@el).find("#hour-active").prop("checked")
        schedule:
          monday: @model.get("hour-monday")
          tuesday: @model.get("hour-tuesday")
          wednesday: @model.get("hour-wednesday")
          thursday: @model.get("hour-thursday")
          friday: @model.get("hour-friday")
          saturday: @model.get("hour-saturday")
          sunday: @model.get("hour-sunday")
        open: $(@el).find("#operating-hour-start-hour").data("DateTimePicker").getDate().format("HH:mm")
        closed: $(@el).find("#operating-hour-stop-hour").data("DateTimePicker").getDate().format("HH:mm")
      @collection.push(new Backbone.Model(data))
      @cancel()

    cancel: ->
      $(@el).find("#garage-create-hour-modal").modal('hide')
      $(@el).undelegate('#garage-create-hour-submit', 'click')
      $(@el).undelegate('#garage-create-hour-cancel', 'click')

    renderLayout: ->
      $(@el).find("#garage-create-hour-modal").remove()
      $(@el).append @template()
      $(@el).find("#garage-create-hour-modal").modal(
        backdrop: 'static'
        keyboard: false
      )

      $(@el).find("#hour-start-minute").numeric()
      $(@el).find("#hour-stop-minute").numeric()
      $(@el).find("#hour-each-minute").numeric()
      $(@el).find("#hour-hour").numeric()

      $(@el).find("#operating-hour-start-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#operating-hour-stop-hour").datetimepicker(
        pickDate: false
      )


      @weekday()

    render: ->
      @renderLayout()
      @

  )
  view
