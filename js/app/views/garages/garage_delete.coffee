define [ "cs!views/veer_view", "text!veer/templates/app/garages/garage_delete.html" ], (VeerView, GarageDeleteTemplate) ->

  view = VeerView.extend(

    template: _.template GarageDeleteTemplate

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render"

    renderLayout: ->
      $(@el).html @template()

    render: ->
      @renderLayout()
      @renderNavigation()
      @
  )
  view

