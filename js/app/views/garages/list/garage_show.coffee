define [ "cs!views/composite_view", "text!veer/templates/app/garages/list/garage_show.html" ], (CompositeView, GarageShowTemplate) ->

  view = CompositeView.extend(

    template: _.template GarageShowTemplate

    initialize: (options) ->
      CompositeView::initialize.call @
      _.bindAll @, "render"

    renderLayout: ->
      $(@el).html @template()

    render: ->
      @renderLayout()
      @
  )
  view

