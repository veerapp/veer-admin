define [ "cs!views/veer_view", "text!veer/templates/app/garages/garage_clone.html", "cs!views/message_view", "backgridpaginator", "backgridselectall" ],
(VeerView, Template, MessageView, backgridpaginator, backgridselectall) ->

  view = VeerView.extend(

    template: _.template Template

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render"

    events:
      "click #garage-submit": "submit"
      "click #cancel": "cancel"

    enable_all: ->
      @garage_name.prop("disabled", false)
      @garage_email.prop("disable", false)
      @garage_street.prop("disabled", false)
      @garage_city.prop("disabled", false)
      @garage_state.prop("disabled", false)
      @garage_zip.prop("disabled", false)
      @garage_phone.prop("disabled", false)
      @garage_web.prop("disabled", false)
      @garage_longitude.prop("disabled", false)
      @garage_latitude.prop("disabled", false)
      @garage_payments.prop("disabled", false)
      @garage_type.prop("disabled", false)
      @garage_active.prop("disabled", false)

      @garage_operating_start_hour.prop("disabled", false)
      @garage_operating_stop_hour.prop("disabled", false)
      @garage_early_bird_start_hour.prop("disabled", false)
      @garage_early_bird_stop_hour.prop("disabled", false)
      @garage_daily_start_hour.prop("disable", false)
      @garage_daily_stop_hour.prop("disable", false)
      @garage_evening_start_hour.prop("disable", false)
      @garage_evening_stop_hour.prop("disable", false)

      @garage_submit.button("reset")

    disable_all: ->
      @garage_name.prop("disabled", true)
      @garage_email.prop("disable", true)
      @garage_street.prop("disabled", true)
      @garage_city.prop("disabled", true)
      @garage_state.prop("disabled", true)
      @garage_zip.prop("disabled", true)
      @garage_phone.prop("disabled", true)
      @garage_web.prop("disabled", true)
      @garage_longitude.prop("disabled", true)
      @garage_latitude.prop("disabled", true)
      @garage_payments.prop("disabled", true)
      @garage_type.prop("disabled", true)
      @garage_active.prop("disabled", true)

      @garage_operating_start_hour.prop("disabled", true)
      @garage_operating_stop_hour.prop("disabled", true)
      @garage_early_bird_start_hour.prop("disabled", true)
      @garage_early_bird_stop_hour.prop("disabled", true)
      @garage_daily_start_hour.prop("disable", true)
      @garage_daily_stop_hour.prop("disable", true)
      @garage_evening_start_hour.prop("disable", true)
      @garage_evening_stop_hour.prop("disable", true)

      @garage_submit.button("loading")

    submit: ->
      @disable_all()
      @model.idAttribute = null
      @model.set "_id":  null

      data = {}
      @model.set "name": @garage_name.val()
      @model.set "active": @garage_active.prop("checked")
      @model.set "geo": [ parseFloat(@garage_longitude.val()), parseFloat(@garage_latitude.val()) ]
      @model.set "web": @garage_web.val()
      @model.set "phone": @garage_phone.val()
      @model.set "email": @garage_email.val()
      @model.set "type": @garage_type.val()
      @model.set "num_spaces": @garage_spaces.val()

      @model.set "address": [
        street: @garage_street.val()
        city: @garage_city.val()
        state: @garage_state.val()
        zip: @garage_zip.val()
      ]

      @model.set "hours":
        operating:
          open: @garage_operating_start_hour.data("DateTimePicker").getDate().format("HH:MM")
          closed: @garage_operating_stop_hour.data("DateTimePicker").getDate().format("HH:MM")
        early_bird:
          start: @garage_early_bird_start_hour.data("DateTimePicker").getDate().format("HH:MM") 
          stop: @garage_early_bird_stop_hour.data("DateTimePicker").getDate().format("HH:MM")
        daily:
          start: @garage_daily_start_hour.data("DateTimePicker").getDate().format("HH:MM")
          stop: @garage_daily_stop_hour.data("DateTimePicker").getDate().format("HH:MM")
        evening:
          start: @garage_evening_start_hour.data("DateTimePicker").getDate().format("HH:MM")
          stop: @garage_evening_stop_hour.data("DateTimePicker").getDate().format("HH:MM")

      @model.set "properties":
        security_cameras: @garage_security_cameras.prop("checked")
        elevators: @garage_elevators.prop("checked")
        wheelchair: @garage_wheelchair.prop("checked")
        ev_charger: @garage_ev_charger.prop("checked")
        wifi_zone: @garage_wifi_zone.prop("checked")

      @model.set "services":
        car_wash: @garage_car_washing.prop("checked")
        gas_fillup: @garage_gas_fillup.prop("checked")
        security_escort: @garage_security_escort.prop("checked")
        flat_tire_assistance: @garage_flat_tire.prop("checked")
        valet: @garage_valet.prop("checked")

      @model.set "payment":
        cash: @garage_payment_cash.prop("checked")
        check: @garage_payment_check.prop("checked")
        credit: @garage_payment_credit.prop("checked")

      @model.save(@model.toJSON(),
        success: (data) =>
          @enable_all()
          Veer.collections.garages.fetch(reset: true)
          _.delay @navigateToGarages, 2000
        error: =>
          @enable_all()
      )

    cancel: ->
      @navigateToGarages()

    navigateToGarages: ->
      Veer.routers.mainRouter.navigate "#garages", trigger: true

    renderLayout: ->
      $(@el).html @template(@model.toJSON())

    renderRuleGrid: ->

    renderExceptionGrid: ->

    render: ->
      @renderLayout()
      @renderNavigation()

      @garage_name = $(@el).find("#garage-name")
      @garage_active = $(@el).find("#garage-active")
      @garage_web = $(@el).find("#garage-web")
      @garage_email = $(@el).find("#garage-email")
      @garage_street = $(@el).find("#garage-street")
      @garage_city = $(@el).find("#garage-city")
      @garage_state = $(@el).find("#garage-state")
      @garage_zip = $(@el).find("#garage-zip")
      @garage_phone = $(@el).find("#garage-phone")
      @garage_longitude = $(@el).find("#garage-longitude")
      @garage_latitude = $(@el).find("#garage-latitude")
      @garage_spaces = $(@el).find("#garage-spaces")

      @garage_zip.numeric()
      @garage_longitude.numeric()
      @garage_latitude.numeric()
      @garage_spaces.numeric()

      @garage_type = $(@el).find("#garage-type")
      @garage_payments = $(@el).find("#garage-payments")
      @garage_submit = $(@el).find("#garage-submit")
      @garage_latitude = $(@el).find("#garage-latitude")
      @garage_longitude = $(@el).find("#garage-longitude")

      @garage_security_cameras = $(@el).find("#garage-security-cameras")
      @garage_elevators = $(@el).find("#garage-elevators")
      @garage_wheelchair = $(@el).find("#garage-wheelchair")
      @garage_ev_charger = $(@el).find("#garage-ev-charger")
      @garage_wifi_zone = $(@el).find("#garage-wifi-zone")
      @garage_car_washing = $(@el).find("#garage-car-washing")
      @garage_gas_fillup = $(@el).find("#garage-gas-fillup")
      @garage_security_escort = $(@el).find("#garage-security-escort")
      @garage_flat_tire = $(@el).find("#garage-flat-tire")
      @garage_valet = $(@el).find("#garage-valet")
      @garage_payment_credit = $(@el).find("#garage-payment-credit")
      @garage_payment_cash = $(@el).find("#garage-payment-cash")
      @garage_payment_check = $(@el).find("#garage-payment-check")

      @garage_operating_start_hour = $(@el).find("#operating-start-hour")
      @garage_operating_stop_hour = $(@el).find("#operating-stop-hour")
      @garage_early_bird_start_hour = $(@el).find("#early-bird-start-hour")
      @garage_early_bird_stop_hour = $(@el).find("#early-bird-stop-hour")
      @garage_daily_start_hour = $(@el).find("#daily-start-hour")
      @garage_daily_stop_hour = $(@el).find("#daily-stop-hour")
      @garage_evening_start_hour = $(@el).find("#evening-start-hour")
      @garage_evening_stop_hour = $(@el).find("#evening-stop-hour")

      $(@el).find("#operating-start-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#operating-stop-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#early-bird-start-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#early-bird-stop-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#daily-start-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#daily-stop-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#evening-start-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#evening-stop-hour").datetimepicker(
        pickDate: false
      )

      @renderRuleGrid()
      @renderExceptionGrid()

      @garage_active.prop("checked", @model.get("active"))
      @garage_security_cameras.prop("checked", @model.get("properties").security_cameras)
      @garage_elevators.prop("checked", @model.get("properties").elevators)
      @garage_wheelchair.prop("checked", @model.get("properties").wheelchair)
      @garage_ev_charger.prop("checked", @model.get("properties").ev_charger)
      @garage_wifi_zone.prop("checked", @model.get("properties").wifi_zone)
      @garage_car_washing.prop("checked", @model.get("services").car_wash)
      @garage_gas_fillup.prop("checked", @model.get("services").gas_fillup)
      @garage_security_escort.prop("checked", @model.get("services").security_escort)
      @garage_flat_tire.prop("checked", @model.get("services").flat_tire_assistance)
      @garage_valet.prop("checked", @model.get("services").valet)
      @garage_payment_credit.prop("checked", @model.get("payment").credit)
      @garage_payment_cash.prop("checked", @model.get("payment").cash)
      @garage_payment_check.prop("checked", @model.get("payment").check)
      @
  )
  view

