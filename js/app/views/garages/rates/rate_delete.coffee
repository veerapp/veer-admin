define [ "cs!views/veer_view", "cs!views/message_view", "text!veer/templates/app/garages/rates/rate_delete.html" ], (VeerView, MessageView, Template) ->
  
  view = VeerView.extend(
    
    template: _.template Template
    
    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render", "renderLayout", "cancel"
      @grid = options.grid
      @errors = []
      
    events:
      "click #database-quiesce-submit": "quiesce_databases"
      "click #database-quiesce-confirm": "quiesce_database_confirm"
      "click #database-quiesce-cancel": "cancel"
      
    enable_all: ->
      $("#database-quiesce-cancel").prop("disabled", false)
      $("#database-quiesce-confirm").prop("disabled", false)
      $("#database-quiesce-submit").button('reset')
      
    disable_all: ->
      $("#database-quiesce-cancel").prop("disabled", true)
      $("#database-quiesce-confirm").prop("disabled", true)
      $("#database-quiesce-submit").button('loading')
      
    cancel: ->
      $("#database-quiesce").modal('hide')
      $(@el).undelegate('#database-quiesce-cancel', 'click')
      $(@el).undelegate('#database-quiesce-confirm', 'click')
      $(@el).undelegate('#database-quiesce-submit', 'click')

    renderLayout: ->
      $("#database-quiesce").remove()
      data =
        items: (model.get("id") for model in @grid.getSelectedModels())
      $("#page").append @template(data)
      $("#database-quiesce").modal(
        backdrop: 'static'
        keyboard: false
      )

    render: ->
      @renderLayout()
      @

  )
  view
