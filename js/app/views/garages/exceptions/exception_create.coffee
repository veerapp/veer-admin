define [ "cs!views/veer_view", "cs!views/message_view", "text!veer/templates/app/garages/exceptions/exception_create.html" ], (VeerView, MessageView, Template) ->
  
  view = VeerView.extend(
    
    template: _.template Template
    
    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render", "renderLayout", "cancel"
      @grid = options.grid
      @errors = []
      
    events:
      "click #garage-create-exception-submit": "submit"
      "click #garage-create-exception-cancel": "cancel"
      
    enable_all: ->
      $(@el).find("#garage-create-exception-cancel").prop("disabled", false)
      $(@el).find("#garage-create-exception-submit").button('reset')
      
    disable_all: ->
      $(@el).find("#garage-create-exception-cancel").prop("disabled", true)
      $(@el).find("#garage-create-exception-submit").button('loading')
      
    submit: ->
      data = 
        id: _.uniqueId()
        name: $(@el).find("#exception-name").val()
        description: if _.isEmpty($(@el).find("#exception-description").val()) then null else $(@el).find("#exception-description").val()
        date: 
          start: $(@el).find("#exception-start-date").data("DateTimePicker").getDate().utc().format()
          stop: $(@el).find("#exception-stop-date").data("DateTimePicker").getDate().utc().format()
        active: $(@el).find("#exception-active").prop("checked")
        inclusive: $(@el).find("#exception-inclusive").prop("checked")
      @collection.push(new Backbone.Model(data))
      @cancel()

    cancel: ->
      $(@el).find("#garage-create-exception-modal").modal('hide')
      $(@el).undelegate('#garage-create-exception-submit', 'click')
      $(@el).undelegate('#garage-create-exception-cancel', 'click')

    renderLayout: ->
      $(@el).find("#garage-create-exception-modal").remove()
      $(@el).append @template()
      $(@el).find("#garage-create-exception-modal").modal(
        backdrop: 'static'
        keyboard: false
      )

      $(@el).find("#exception-start-date").datetimepicker(
        pickDate: true
        pickTime: true
      )

      $(@el).find("#exception-stop-date").datetimepicker(
        pickDate: true
        pickTime: true
      )

    render: ->
      @renderLayout()
      @

  )
  view
