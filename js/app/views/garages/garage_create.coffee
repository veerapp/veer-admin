define [ "cs!views/veer_view", "text!veer/templates/app/garages/garage_create.html", "cs!views/message_view", "numeric", 
"cs!views/garages/rates/rate_create", 
"cs!views/garages/rates/rate_delete", 
"cs!views/garages/hours/hour_create", 
"cs!views/garages/venues/venue_create",
"cs!views/garages/exceptions/exception_create",
"cs!views/garages/exceptions/exception_delete", "cs!collections/rates", "cs!collections/exceptions",
"cs!collections/venues", "cs!collections/operating.hours" ],
(VeerView, Template, MessageView, numeric, GarageAddRateView, GarageDeleteRateView, GarageAddOperatingHourView, GarageAddVenueView,
  GarageAddExceptionView, GarageDeleteExceptionView, RateCollection, ExceptionCollection, VenueCollection, OperatingHourCollection) ->

  view = VeerView.extend(

    template: _.template Template

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render", "successGeo", "errorGeo"
      @views = []
      @rates = new RateCollection()
      @exceptions = new ExceptionCollection()
      @venues = new VenueCollection()
      @hours = new OperatingHourCollection()

    events:
      "click #garage-add-rate": "add_rate"
      "click #garage-delete-rate": "delete_rate"
      
      "click #garage-add-exception": "add_exception"
      "click #garage-delete-exception": "delete_exception"
      
      "click #garage-add-venue": "add_venue"
      "click #garage-delete-venue": "delete_venue"

      "click #garage-add-operating-hour": "add_operating_hour"
      "click #garage-delete-operating-hour": "delete_operating_hour"

      "click #garage-submit": "submit"
      "click #cancel": "cancel"

    enable_all: ->
      @garage_name.prop("disabled", false)
      @garage_email.prop("disable", false)
      @garage_street.prop("disabled", false)
      @garage_city.prop("disabled", false)
      @garage_state.prop("disabled", false)
      @garage_zip.prop("disabled", false)
      @garage_phone.prop("disabled", false)
      @garage_web.prop("disabled", false)
      @garage_longitude.prop("disabled", false)
      @garage_latitude.prop("disabled", false)
      @garage_payments.prop("disabled", false)
      @garage_type.prop("disabled", false)
      @garage_active.prop("disabled", false)
      @garage_submit.button("reset")

    disable_all: ->
      @garage_name.prop("disabled", true)
      @garage_email.prop("disable", true)
      @garage_street.prop("disabled", true)
      @garage_city.prop("disabled", true)
      @garage_state.prop("disabled", true)
      @garage_zip.prop("disabled", true)
      @garage_phone.prop("disabled", true)
      @garage_web.prop("disabled", true)
      @garage_longitude.prop("disabled", true)
      @garage_latitude.prop("disabled", true)
      @garage_payments.prop("disabled", true)
      @garage_type.prop("disabled", true)
      @garage_active.prop("disabled", true)
      @garage_submit.button("loading")

    add_operating_hour: ->
      view = new GarageAddOperatingHourView(
        el: "body"
        collection: @operating_hour_grid.collection
      )
      @views.push(view)
      view.render()
      false

    delete_operating_hour: ->
      $(".btn-group").removeClass("open")
      _.each @operating_hour_grid.getSelectedModels(), (model) ->
        model.idAttribute = null
        model.destroy()
      false

    add_venue: ->
      view = new GarageAddVenueView(
        el: "body"
        collection: @venue_grid.collection
      )
      @views.push(view)
      view.render()
      false

    delete_venue: ->
      $(".btn-group").removeClass("open")
      _.each @venue_grid.getSelectedModels(), (model) ->
        model.idAttribute = null
        model.destroy()
      false

    add_exception: ->
      view = new GarageAddExceptionView(
        el: "body"
        collection: @exception_grid.collection
      )
      @views.push(view)
      view.render()
      false

    delete_exception: ->
      $(".btn-group").removeClass("open")
      _.each @exception_grid.getSelectedModels(), (model) ->
        model.idAttribute = null
        model.destroy()
      false

    add_rate: ->
      view = new GarageAddRateView(
        el: "body"
        collection: @rate_grid.collection
      )
      @views.push(view)
      view.render()
      false

    delete_rate: ->
      $(".btn-group").removeClass("open")
      _.each @rate_grid.getSelectedModels(), (model) ->
        model.idAttribute = null
        model.destroy()
      false

    submit: ->
      ADDRESS = @garage_street.val() + " " + @garage_city.val() + ", " + @garage_state.val() + " " + @garage_zip.val()
      $.ajax(
        type: "GET"
        dataType: "jsonp"
        cache: true
        url: "https://www.mapquestapi.com/geocoding/v1/address?key=Fmjtd%7Cluub2q0bll%2C85%3Do5-9u7590&location=" + ADDRESS + "&addressdetails=1"
        success: @successGeo
        error: @errorGeo
      )
      false

    errorGeo: ->
      @enable_all()
      view = new MessageView(
        css: "alert-error"
        message: "Error geocoding address. Please check address info"
        id: "#garage-message"
        stick: true
      )
      view.render()

    successGeo: (geoData) ->
      @disable_all()
      @model.idAttribute = null

      data = {}
      data["name"] = @garage_name.val()
      data["active"] = @garage_active.prop("checked")
      data["geo"] = [ parseFloat(@garage_longitude.val()), parseFloat(@garage_latitude.val()) ]
      data["web"] = @garage_web.val()
      data["phone"] = @garage_phone.val()
      data["email"] = @garage_email.val()
      data["type"] = @garage_type.val()
      data["num_spaces"] = if _.isEmpty(@garage_spaces.val()) then null else parseInt(@garage_spaces.val())
      data["event"] = {}
      data.event.cost = if _.isEmpty(@garage_event_cost.val()) then null else parseInt(@garage_event_cost.val())
      data.event.start = if _.isEmpty(@garage_event_rate_start_buffer.val()) then null else parseInt(@garage_event_rate_start_buffer.val())
      data.event.stop = if _.isEmpty(@garage_event_rate_stop_buffer.val()) then null else parseInt(@garage_event_rate_stop_buffer.val())

      data["address"] = [
        street: @garage_street.val()
        city: @garage_city.val()
        state: @garage_state.val()
        zip: @garage_zip.val()
        countryCode: geoData.results[0].locations[0].adminArea1
        geo: [ geoData.results[0].locations[0].latLng.lng, geoData.results[0].locations[0].latLng.lat ]
      ]

      data["properties"] =
        security_cameras: @garage_security_cameras.prop("checked")
        elevators: @garage_elevators.prop("checked")
        wheelchair: @garage_wheelchair.prop("checked")
        ev_charger: @garage_ev_charger.prop("checked")
        wifi_zone: @garage_wifi_zone.prop("checked")

      data["services"] = 
        car_wash: @garage_car_washing.prop("checked")
        gas_fillup: @garage_gas_fillup.prop("checked")
        security_escort: @garage_security_escort.prop("checked")
        flat_tire_assistance: @garage_flat_tire.prop("checked")
        valet: @garage_valet.prop("checked")

      data["payment"] =
        cash: @garage_payment_cash.prop("checked")
        check: @garage_payment_check.prop("checked")
        credit: @garage_payment_credit.prop("checked")

      data["rates"] = @rates.toJSON()
      data["exceptions"] = @exceptions.toJSON()
      data["hours"] = @hours.toJSON()
      data["venues"] = @venues.toJSON()

      data["operator_id"] = @garage_operator.find("option:selected").attr("id")

      # rate card picture
      rateCardPic = new FormData()
      rateCardPic.append "file-0", document.getElementById('garage-rate-chart').files[0]

      # garage entrance picture
      entrancePic = new FormData()
      entrancePic.append "file-1", document.getElementById('garage-picture').files[0]

      RATE_CARD_PIC = if _.isUndefined(document.getElementById('garage-rate-chart').files[0]) then null else rateCardPic
      ENTRANCE_PIC = if _.isUndefined(document.getElementById('garage-picture').files[0]) then null else entrancePic

      # upload rate card picture
      @newFile RATE_CARD_PIC, (err, rateCardFile) =>

        # upload entrance picture
        @newFile ENTRANCE_PIC, (err, entranceFile) =>

          data["entrance_pic_id"] = if _.isNull(entranceFile) then null else entranceFile._id
          data["rate_card_pic_id"] = if _.isNull(rateCardFile) then null else rateCardFile._id

          console.log "creating garage: " + JSON.stringify(data, null, 2)

          # create the garage
          Veer.collections.garages.create(data,
            wait: true
            success: =>
              @navigateToGarages()
            error: (model, err) =>
              @scrollToTop()
              view = new MessageView(
                id: "#garage-message"
                css: "alert-danger"
                message: err.responseText
                stick: true
              )
              view.render()
              @enable_all()
          )

    cancel: ->
      @navigateToGarages()

    navigateToGarages: ->
      Veer.routers.mainRouter.navigate "#garages", trigger: true

    renderLayout: ->
      $(@el).html @template()

    renderVenueGrid: ->
      columns = [
        {
          name: ""
          cell: "select-row"
          sortable: false
          headerCell: "select-all"
        }
        {
          name: "active"
          label: "Active"
          editable: true
          sortable: false
          cell: "boolean"
        }
        {
          name: "name"
          label: "Name"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.append $("<a href='#venues/" + encodeURIComponent(@model.get("_id")) + "'>",
                href: @column.get("label")
                class: "uri-cell sortable renderable"
                title: @column.get("label")
                target: ""
              ).text(@model.get("name"))
              @
          )
        }
      ]

      # Initialize a new Grid instance
      @venue_grid = new Backgrid.Grid(
        columns: columns
        collection: @venues
      )

      # Render the grid and attach the root to your HTML document
      $(@el).find("#venue-container").append @venue_grid.render().el

      # Initialize the paginator
      paginator = new Backgrid.Extension.Paginator(
        
        # If you anticipate a large number of pages, you can adjust
        # the number of page handles to show. The sliding window
        # will automatically show the next set of page handles when
        # you click next at the end of a window.
        windowSize: 20 # Default is 10
        
        # Used to multiple windowSize to yield a number of pages to slide,
        # in the case the number is 5
        slideScale: 0.25 # Default is 0.5
        
        # Whether sorting should go back to the first page
        goBackFirstOnSort: false # Default is true
        collection: @venues
      )

      # Render the paginator
      $(@el).find("#venue-container").after(paginator.render().el);

    renderOperatingHoursGrid: ->
      columns = [
        {
          name: ""
          cell: "select-row"
          sortable: false
          headerCell: "select-all"
        }
        {
          name: "active"
          label: "Active"
          editable: true
          sortable: false
          cell: "boolean"
        }
        {
          name: "schedule"
          label: "Days"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              values = []
              switcher = 
                "monday": "mon"
                "tuesday": "tues"
                "wednesday": "wed"
                "thursday": "thurs"
                "friday": "fri"
                "saturday": "sat"
                "sunday": "sun"
              for k,v of this.model.get("schedule")
                values.push(switcher[k]) if v
              @$el.html values.toString()
              @
          )
        }
        {
          name: "type"
          label: "Type"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.html "range:hour"
              @
          )
        }
        {
          name: "from"
          label: "From"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.html @model.get("open")
              @
          )
        }
        {
          name: "to"
          label: "To"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.html @model.get("closed")
              @
          )
        }
      ]

      # Initialize a new Grid instance
      @operating_hour_grid = new Backgrid.Grid(
        columns: columns
        collection: @hours
      )

      # Render the grid and attach the root to your HTML document
      $(@el).find("#operating-hours-container").append @operating_hour_grid.render().el

      # Initialize the paginator
      paginator = new Backgrid.Extension.Paginator(collection: @hours)

      # Render the paginator
      $(@el).find("#operating-hours-container").after(paginator.render().el);

    renderRateGrid: ->
      columns = [
        {
          name: ""
          cell: "select-row"
          sortable: false
          headerCell: "select-all"
        }
        {
          name: "active"
          label: "Active"
          editable: true
          sortable: false
          cell: "boolean"
        }
        {
          name: "schedule"
          label: "Days"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              values = []
              switcher = 
                "monday": "mon"
                "tuesday": "tues"
                "wednesday": "wed"
                "thursday": "thurs"
                "friday": "fri"
                "saturday": "sat"
                "sunday": "sun"
              for k,v of this.model.get("schedule")
                values.push(switcher[k]) if v
              @$el.html values.toString()
              @
          )
        }
        {
          name: "type"
          label: "Type"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              unless _.isUndefined(@model.get("type").range_hour)
                if @model.get("type").range_hour.active
                  @$el.html "range:hour"
              unless _.isUndefined(@model.get("type").range_min)
                if @model.get("type").range_min.active
                  @$el.html "range:min"
              unless _.isUndefined(@model.get("type").each)
                if @model.get("type").each.active
                  @$el.html "each:min"
              unless _.isUndefined(@model.get("type").flat)
                if @model.get("type").flat.active
                  @$el.html "flat"
              unless _.isUndefined(@model.get("type").early_bird)
                if @model.get("type").early_bird.active
                  @$el.html "range:early_bird"
              unless _.isUndefined(@model.get("type").event)
                if @model.get("type").event.active
                  @$el.html "range:event"
              @
          )
        }
        {
          name: "from"
          label: "From"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              unless _.isUndefined(@model.get("type").range_hour)
                if @model.get("type").range_hour.active
                  @$el.html @model.get("type").range_hour.gt
              unless _.isUndefined(@model.get("type").range_min)
                if @model.get("type").range_min.active
                  @$el.html @model.get("type").range_min.gt + " min"
              unless _.isUndefined(@model.get("type").each)
                if @model.get("type").each.active
                  @$el.html @model.get("type").each.minute + " min"
              unless _.isUndefined(@model.get("type").flat)
                if @model.get("type").flat.active
                  @$el.html "--"
              unless _.isUndefined(@model.get("type").early_bird)
                if @model.get("type").early_bird.active
                  @$el.html @model.get("type").early_bird.start_gt + " - " +  @model.get("type").early_bird.start_lt
              unless _.isUndefined(@model.get("type").event)
                if @model.get("type").event.active
                  @$el.html @model.get("type").event.gt
              @
          )
        }
        {
          name: "to"
          label: "To"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              unless _.isUndefined(@model.get("type").range_hour)
                if @model.get("type").range_hour.active
                  @$el.html @model.get("type").range_hour.lt
              unless _.isUndefined(@model.get("type").range_min)
                if @model.get("type").range_min.active
                  @$el.html @model.get("type").range_min.lt + " min"
              unless _.isUndefined(@model.get("type").each)
                if @model.get("type").each.active
                  @$el.html @model.get("type").each.minute + " min"
              unless _.isUndefined(@model.get("type").flat)
                if @model.get("type").flat.active
                  @$el.html "--"
              unless _.isUndefined(@model.get("type").early_bird)
                if @model.get("type").early_bird.active
                  @$el.html @model.get("type").early_bird.stop_gt + " - " +  @model.get("type").early_bird.stop_lt
              unless _.isUndefined(@model.get("type").event)
                if @model.get("type").event.active
                  @$el.html @model.get("type").event.lt
              @
          )
        }
        {
          name: "cost"
          label: "Cost"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.html "$" + @model.get("cost")
              @
          )
        }
      ]

      # Initialize a new Grid instance
      @rate_grid = new Backgrid.Grid(
        columns: columns
        collection: @rates
      )

      # Render the grid and attach the root to your HTML document
      $(@el).find("#rate-container").append @rate_grid.render().el

      # Initialize the paginator
      paginator = new Backgrid.Extension.Paginator(collection: @rates)

      # Render the paginator
      $(@el).find("#rate-container").after(paginator.render().el);

    renderExceptionGrid: ->
      columns = [
        {
          name: ""
          cell: "select-row"
          sortable: false
          headerCell: "select-all"
        }
        {
          name: "active"
          label: "Active"
          editable: true
          sortable: false
          cell: "boolean"
        }
        {
          name: "name"
          label: "Name"
          editable: false
          sortable: false
          cell: "string"
        }
        {
          name: "from"
          label: "From"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.html moment(@model.get("date").start).utc().format("LLLL")
              @
          )
        }
        {
          name: "to"
          label: "To"
          editable: false
          sortable: false
          cell: Backgrid.Cell.extend(
            className: "string-cell"
            formatter: Backgrid.StringFormatter
            render: ->
              @$el.html moment(@model.get("date").stop).utc().format("LLLL")
              @
          )
        }
      ]

      # Initialize a new Grid instance
      @exception_grid = new Backgrid.Grid(
        columns: columns
        collection: @exceptions
      )

      # Render the grid and attach the root to your HTML document
      $(@el).find("#exception-container").append @exception_grid.render().el

      # Initialize the paginator
      paginator = new Backgrid.Extension.Paginator(collection: @exceptions)

      # Render the paginator
      $(@el).find("#exception-container").after(paginator.render().el)

    render: ->
      @renderLayout()
      @renderNavigation()

      @garage_name = $(@el).find("#garage-name")
      @garage_active = $(@el).find("#garage-active")
      @garage_web = $(@el).find("#garage-web")
      @garage_email = $(@el).find("#garage-email")
      @garage_street = $(@el).find("#garage-street")
      @garage_city = $(@el).find("#garage-city")
      @garage_state = $(@el).find("#garage-state")
      @garage_zip = $(@el).find("#garage-zip")
      @garage_phone = $(@el).find("#garage-phone")
      @garage_longitude = $(@el).find("#garage-longitude")
      @garage_latitude = $(@el).find("#garage-latitude")
      @garage_spaces = $(@el).find("#garage-spaces")

      @garage_event_rate_start_buffer = $(@el).find("#garage-event-rate-start-buffer")
      @garage_event_rate_stop_buffer = $(@el).find("#garage-event-rate-stop-buffer")
      @garage_event_cost = $(@el).find("#garage-event-cost")

      @garage_zip.numeric()
      @garage_longitude.numeric()
      @garage_latitude.numeric()
      @garage_spaces.numeric()
      @garage_event_rate_start_buffer.numeric()
      @garage_event_rate_stop_buffer.numeric()
      @garage_event_cost.numeric()

      @garage_type = $(@el).find("#garage-type")
      @garage_payments = $(@el).find("#garage-payments")
      @garage_submit = $(@el).find("#garage-submit")
      @garage_latitude = $(@el).find("#garage-latitude")
      @garage_longitude = $(@el).find("#garage-longitude")
      @garage_rate_card = $(@el).find("#garage-rate-chart")

      @garage_security_cameras = $(@el).find("#garage-security-cameras")
      @garage_elevators = $(@el).find("#garage-elevators")
      @garage_wheelchair = $(@el).find("#garage-wheelchair")
      @garage_ev_charger = $(@el).find("#garage-ev-charger")
      @garage_wifi_zone = $(@el).find("#garage-wifi-zone")
      @garage_car_washing = $(@el).find("#garage-car-washing")
      @garage_gas_fillup = $(@el).find("#garage-gas-fillup")
      @garage_security_escort = $(@el).find("#garage-security-escort")
      @garage_flat_tire = $(@el).find("#garage-flat-tire")
      @garage_valet = $(@el).find("#garage-valet")
      @garage_payment_credit = $(@el).find("#garage-payment-credit")
      @garage_payment_cash = $(@el).find("#garage-payment-cash")
      @garage_payment_check = $(@el).find("#garage-payment-check")

      @garage_message = $(@el).find("#garage-message")
      @garage_operator = $(@el).find("#garage-operator")
      @garage_picture = $(@el).find("#garage-picture")

      @renderRateGrid()
      @renderVenueGrid()
      @renderExceptionGrid()
      @renderOperatingHoursGrid()

      Veer.collections.operators.each (operator) =>
        $(@el).find("#garage-operator").append("<option id='" + operator.get("_id") + "'>" + operator.get("name") + "</option>")
      
      @
  )
  view

