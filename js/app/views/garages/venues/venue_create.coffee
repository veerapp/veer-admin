define [ "cs!views/veer_view", "cs!views/message_view", "text!veer/templates/app/garages/venues/venue_create.html", "numeric" ], (VeerView, MessageView, Template, numeric) ->
  
  view = VeerView.extend(
    
    template: _.template Template
    
    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render", "renderLayout", "cancel"
      @grid = options.grid
      @el = options.el
      @errors = []
      @model = new Backbone.Model()
      
    events:
      "click #rate-weekday": "weekday"
      "click #rate-weekend": "weekend"
      "click #rate-every-day": "every_day"
      "click #rate-monday": "monday"
      "click #rate-tuesday": "tuesday"
      "click #rate-wednesday": "wednesday"
      "click #rate-thursday": "thursday"
      "click #rate-friday": "friday"
      "click #rate-saturday": "saturday"
      "click #rate-sunday": "sunday"
      "change #rate-start-hour": "start_hour"
      "change #rate-stop-hour": "stop_hour"
      "change #rate-start-minute": "start_minute"
      "change #rate-stop-minute": "stop_minute"
      "change #rate-each-minute": "each_minute"
      "change #rate-type": "rate_type"
      "click #garage-create-rate-submit": "submit"
      "click #garage-create-rate-cancel": "cancel"
      
    enable_all: ->
      $(@el).find("#garage-create-rate-cancel").prop("disabled", false)
      $(@el).find("#garage-create-rate-submit").button('reset')
      
    disable_all: ->
      $(@el).find("#garage-create-rate-cancel").prop("disabled", true)
      $(@el).find("#garage-create-rate-submit").button('loading')

    validate: ->

    weekday: ->
      $(@el).find(".weekday").show()
      $(@el).find(".weekend").hide()

      @model.set "rate-monday": $(@el).find("#rate-monday").prop("checked", true).prop("checked")
      @model.set "rate-tuesday": $(@el).find("#rate-tuesday").prop("checked", true).prop("checked")
      @model.set "rate-wednesday": $(@el).find("#rate-wednesday").prop("checked", true).prop("checked")
      @model.set "rate-thursday": $(@el).find("#rate-thursday").prop("checked", true).prop("checked")
      @model.set "rate-friday": $(@el).find("#rate-friday").prop("checked", true).prop("checked")
      @model.set "rate-saturday": $(@el).find("#rate-saturday").prop("checked", false).prop("checked")
      @model.set "rate-sunday": $(@el).find("#rate-sunday").prop("checked", false).prop("checked")

    weekend: ->
      $(@el).find(".weekday").hide()
      $(@el).find(".weekend").show()

      @model.set "rate-monday": $(@el).find("#rate-monday").prop("checked", false).prop("checked")
      @model.set "rate-tuesday": $(@el).find("#rate-tuesday").prop("checked", false).prop("checked")
      @model.set "rate-wednesday": $(@el).find("#rate-wednesday").prop("checked", false).prop("checked")
      @model.set "rate-thursday": $(@el).find("#rate-thursday").prop("checked", false).prop("checked")
      @model.set "rate-friday": $(@el).find("#rate-friday").prop("checked", false).prop("checked")
      @model.set "rate-saturday": $(@el).find("#rate-saturday").prop("checked", true).prop("checked")
      @model.set "rate-sunday": $(@el).find("#rate-sunday").prop("checked", true).prop("checked")

    every_day: ->
      $(@el).find(".weekday").show()
      $(@el).find(".weekend").show()

      @model.set "rate-monday": $(@el).find("#rate-monday").prop("checked", true).prop("checked")
      @model.set "rate-tuesday": $(@el).find("#rate-tuesday").prop("checked", true).prop("checked")
      @model.set "rate-wednesday": $(@el).find("#rate-wednesday").prop("checked", true).prop("checked")
      @model.set "rate-thursday": $(@el).find("#rate-thursday").prop("checked", true).prop("checked")
      @model.set "rate-friday": $(@el).find("#rate-friday").prop("checked", true).prop("checked")
      @model.set "rate-saturday": $(@el).find("#rate-saturday").prop("checked", true).prop("checked")
      @model.set "rate-sunday": $(@el).find("#rate-sunday").prop("checked", true).prop("checked")

    monday: ->
      @model.set "rate-monday": $(@el).find("#rate-monday").prop("checked")
      @validate()

    tuesday: ->
      @model.set "rate-tuesday": $(@el).find("#rate-tuesday").prop("checked")
      @validate()

    wednesday: ->
      @model.set "rate-wednesday": $(@el).find("#rate-wednesday").prop("checked")
      @validate()

    thursday: ->
      @model.set "rate-thursday": $(@el).find("#rate-thursday").prop("checked")
      @validate()

    friday: ->
      @model.set "rate-friday": $(@el).find("#rate-friday").prop("checked")
      @validate()

    saturday: ->
      @model.set "rate-saturday": $(@el).find("#rate-saturday").prop("checked")
      @validate()

    sunday: ->
      @model.set "rate-sunday": $(@el).find("#rate-sunday").prop("checked")
      @validate()

    start_hour: ->
      @model.set "rate-start-hour": $(@el).find("#rate-start-hour").data("DateTimePicker").getDate().format("HH:mm")
      @validate()

    stop_hour: ->
      @model.set "rate-stop-hour": $(@el).find("#rate-stop-hour").data("DateTimePicker").getDate().format("HH:mm")
      @validate()

    start_minute: ->
      @model.set "rate-start-minute": $(@el).find("#rate-start-minute").val()
      @validate()

    stop_minute: ->
      @model.set "rate-stop-minute": $(@el).find("#rate-stop-minute").val()
      @validate()

    each_minute: ->
      @model.set "type-each-minutes": $(@el).find("#rate-each-minute").val()
      @validate()

    rate_type: ->
      switch $(@el).find("#rate-type :selected").attr("type")
        when "range-hour"
          $(@el).find(".show_hour_range").show()
          $(@el).find(".show_minute_range").hide()
          $(@el).find(".show_each").hide()
          $(@el).find(".show_early_bird_range").hide()
          $(@el).find(".show_event").hide()

          @model.set "type-range-hour-active": true
          @model.set "type-range-minute-active": false
          @model.set "type-each-minute-active": false
          @model.set "type-early-bird-active": false

          @model.set "type-range-hour-gt": $(@el).find("#rate-start-hour").data("DateTimePicker").getDate().format("HH:mm")
          @model.set "type-range-hour-lt": $(@el).find("#rate-stop-hour").data("DateTimePicker").getDate().format("HH:mm")
          @model.set "type-range-minute-gt": null
          @model.set "type-range-minute-lt": null

        when "range-minute"
          $(@el).find(".show_hour_range").hide()
          $(@el).find(".show_minute_range").show()
          $(@el).find(".show_each").hide()
          $(@el).find(".show_early_bird_range").hide()
          $(@el).find(".show_event").hide()

          @model.set "type-range-hour-active": false
          @model.set "type-range-minute-active": true
          @model.set "type-each-minute-active": false
          @model.set "type-early-bird-active": false

          @model.set "type-range-hour-gt": null
          @model.set "type-range-hour-lt": null
          @model.set "type-range-minute-gt": $(@el).find("#rate-start-minute").val()
          @model.set "type-range-minute-lt": $(@el).find("#rate-stop-minute").val()

        when "each"
          $(@el).find(".show_hour_range").hide()
          $(@el).find(".show_minute_range").hide()
          $(@el).find(".show_each").show()
          $(@el).find(".show_early_bird_range").hide()
          $(@el).find(".show_event").hide()

          @model.set "type-range-hour-active": false
          @model.set "type-range-minute-active": false
          @model.set "type-each-minute-active": true
          @model.set "type-early-bird-active": false

          @model.set "type-each-minutes": $(@el).find("#rate-each-minute").val()

        when "flat"
          $(@el).find(".show_hour_range").hide()
          $(@el).find(".show_minute_range").hide()
          $(@el).find(".show_each").hide()
          $(@el).find(".show_early_bird_range").hide()
          $(@el).find(".show_event").hide()

        when "early_bird"
          $(@el).find(".show_hour_range").hide()
          $(@el).find(".show_minute_range").hide()
          $(@el).find(".show_each").hide()
          $(@el).find(".show_early_bird_range").show()
          $(@el).find(".show_event").hide()

          @model.set "type-range-hour-active": false
          @model.set "type-range-minute-active": false
          @model.set "type-each-minute-active": false
          @model.set "type-early-bird-active": true

        when "event"
          $(@el).find(".show_hour_range").hide()
          $(@el).find(".show_minute_range").hide()
          $(@el).find(".show_each").hide()
          $(@el).find(".show_early_bird_range").hide()
          $(@el).find(".show_event").show()
        

    submit: ->
      data =
        id: _.uniqueId()
        cost: if _.isEmpty($(@el).find("#rate-cost").val()) then null else parseInt($(@el).find("#rate-cost").val())
        active: $(@el).find("#rate-active").prop("checked")
        schedule:
          monday: @model.get("rate-monday")
          tuesday: @model.get("rate-tuesday")
          wednesday: @model.get("rate-wednesday")
          thursday: @model.get("rate-thursday")
          friday: @model.get("rate-friday")
          saturday: @model.get("rate-saturday")
          sunday: @model.get("rate-sunday")

      if @model.get("type-range-hour-active")
        data.type = 
          range_hour: 
            active: @model.get("type-range-hour-active")
            gt: $(@el).find("#rate-start-hour").data("DateTimePicker").getDate().format("HH:mm")
            lt: $(@el).find("#rate-stop-hour").data("DateTimePicker").getDate().format("HH:mm")

      if @model.get("type-range-minute-active")
        data.type = 
          range_min:
            active: @model.get("type-range-minute-active")
            gt: $(@el).find("#rate-start-minute").val()
            lt: $(@el).find("#rate-stop-minute").val()

      if @model.get("type-each-minute-active")
        data.type =
          each:
            active: @model.get("type-each-minute-active")
            minute: @model.get("type-each-minutes")

      if @model.get("type-flat-active")
        data.type = 
          flat:
            active: @model.get("type-flat-active")

      if @model.get("type-early-bird-active")
        data.type =
          early_bird:
            active: @model.get("type-early-bird-active")
            start_gt: $(@el).find("#rate-start-gt-hour").data("DateTimePicker").getDate().format("HH:mm")
            start_lt: $(@el).find("#rate-start-lt-hour").data("DateTimePicker").getDate().format("HH:mm")
            stop_gt: $(@el).find("#rate-stop-gt-hour").data("DateTimePicker").getDate().format("HH:mm")
            stop_lt: $(@el).find("#rate-stop-lt-hour").data("DateTimePicker").getDate().format("HH:mm")

      if @model.get("type-event-active")
        data.type = 
          event:
            active: @model.get("type-event-active")
            gt: $(@el).find("#event-rate-start-hour").data("DateTimePicker").getDate().format("HH:mm")
            lt: $(@el).find("#event-rate-stop-hour").data("DateTimePicker").getDate().format("HH:mm")

      @collection.push(new Backbone.Model(data))
      @cancel()

    cancel: ->
      $(@el).find("#garage-create-rate-modal").modal('hide')
      $(@el).undelegate('#garage-create-rate-submit', 'click')
      $(@el).undelegate('#garage-create-rate-cancel', 'click')

    renderLayout: ->
      $(@el).find("#garage-create-rate-modal").remove()
      $(@el).append @template()
      $(@el).find("#garage-create-rate-modal").modal(
        backdrop: 'static'
        keyboard: false
      )

      $(@el).find("#rate-start-minute").numeric()
      $(@el).find("#rate-stop-minute").numeric()
      $(@el).find("#rate-each-minute").numeric()
      $(@el).find("#rate-hour").numeric()

      $(@el).find("#rate-start-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#rate-stop-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#rate-start-gt-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#rate-start-lt-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#rate-stop-gt-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#rate-stop-lt-hour").datetimepicker(
        pickDate: false
      )

      $(@el).find("#event-rate-start-hour").datetimepicker(
        pickDate: true
        pickTime: true
      )

      $(@el).find("#event-rate-stop-hour").datetimepicker(
        pickDate: true
        pickTime: true
      )


      @rate_type()
      @weekday()

    render: ->
      @renderLayout()
      @

  )
  view
