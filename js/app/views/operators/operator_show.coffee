define [ "cs!views/veer_view", "text!veer/templates/app/operators/operator_show.html", "cs!views/message_view", "backgridpaginator", "backgridselectall" ],
(VeerView, Template, MessageView, backgridpaginator, backgridselectall) ->

  view = VeerView.extend(

    template: _.template Template

    initialize: (options) ->
      VeerView::initialize.call @
      _.bindAll @, "render"
      @views = []

    events:
      "click #operator-submit": "submit"
      "click #operator-delete": "delete"
      "click #cancel": "cancel"

    enable_all: ->
      @operator_name.prop("disabled", false)
      @operator_street.prop("disabled", false)
      @operator_city.prop("disabled", false)
      @operator_state.prop("disabled", false)
      @operator_zip.prop("disabled", false)
      @operator_phone.prop("disabled", false)
      @operator_web.prop("disabled", false)
      @operator_longitude.prop("disabled", false)
      @operator_latitude.prop("disabled", false)
      @operator_active.prop("disabled", false)
      @operator_submit.button("reset")

    disable_all: ->
      @operator_name.prop("disabled", true)
      @operator_street.prop("disabled", true)
      @operator_city.prop("disabled", true)
      @operator_state.prop("disabled", true)
      @operator_zip.prop("disabled", true)
      @operator_phone.prop("disabled", true)
      @operator_web.prop("disabled", true)
      @operator_longitude.prop("disabled", true)
      @operator_latitude.prop("disabled", true)
      @operator_active.prop("disabled", true)
      @operator_submit.button("loading")

    submit: ->
      @disable_all()
      @model.set "name": @operator_name.val()
      @model.set "active": @operator_active.prop("checked")
      @model.set "geo": [ parseFloat(@operator_longitude.val()), parseFloat(@operator_latitude.val()) ]
      @model.set "web": @operator_web.val()
      @model.set "phone": @operator_phone.val()

      @model.set "address": [
        street: @operator_street.val()
        city: @operator_city.val()
        state: @operator_state.val()
        zip: @operator_zip.val()
      ]

      @model.save(@model.toJSON(),
        wait: true
        success: (data) =>
          @navigateToOperators()
        error: (model, err) =>
          view = new MessageView(
            id: "#operator-message"
            css: "alert-danger"
            message: err.responseText
            stick: true
          )
          view.render()
          @enable_all()
      )

    cancel: ->
      @navigateToVenues()

    delete: ->
      @disable_all()
      @model.destroy(
        wait: true
        success: =>
          @navigateToVenues()
        error: (model, err) =>
          @scrollToTop()
          view = new MessageView(
            id: "#operator-message"
            css: "alert-danger"
            message: err.responseText
            stick: true
          )
          view.render()
          @enable_all()
      )

    renderLayout: ->
      $(@el).html @template(@model.toJSON())

    render: ->
      VeerView::render.call @

      @operator_name = $(@el).find("#operator-name")
      @operator_active = $(@el).find("#operator-active")
      @operator_web = $(@el).find("#operator-web")
      @operator_street = $(@el).find("#operator-street")
      @operator_city = $(@el).find("#operator-city")
      @operator_state = $(@el).find("#operator-state")
      @operator_zip = $(@el).find("#operator-zip")
      @operator_phone = $(@el).find("#operator-phone")
      @operator_longitude = $(@el).find("#operator-longitude")
      @operator_latitude = $(@el).find("#operator-latitude")
      @operator_submit = $(@el).find("#operator-submit")

      @operator_zip.numeric()
      @operator_longitude.numeric()
      @operator_latitude.numeric()

      @operator_active.prop("checked", @model.get("active"))

      @
  )
  view

