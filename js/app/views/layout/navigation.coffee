define [ "cs!veer/js/app/config", "cs!views/composite_view", "text!veer/templates/app/layout/navigation.html" ], (Config, CompositeView, NavigationTemplate) ->

  view = CompositeView.extend(

    template: _.template NavigationTemplate

    initialize: ->
      _.bindAll @, "render"

    renderLayout: ->
      $(@el).html @template()

    render: ->
      @renderLayout()
      @

  )
  view
